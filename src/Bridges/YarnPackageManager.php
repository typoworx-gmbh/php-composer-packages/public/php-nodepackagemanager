<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager\Bridges;

class YarnPackageManager extends AbstractPackageManager
{
    public function supportsWorkspaces() : bool
    {
        return true;
    }

    public function installPackage(string $packageName) : PackageManagerResponse
    {
        return $this->runCommand(
            sprintf(
                'yarn --cwd %s --modules-folder %s/yarn-test/node_modules add %s',
                escapeshellarg($this->path),
                escapeshellarg($this->path),
                escapeshellarg($packageName)
            )
        );
    }

    public function removePackage(string $packageName) : PackageManagerResponse
    {
        return $this->runCommand(sprintf(
            'yarn remove %s',
            escapeshellarg($packageName)
        ));
    }

    public function updatePackage(string $packageName) : PackageManagerResponse
    {
        return $this->runCommand(sprintf(
            'yarn upgrade %s',
            escapeshellarg($packageName)
        ));
    }
}
