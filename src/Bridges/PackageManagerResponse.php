<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager\Bridges;

use Throwable;

class PackageManagerResponse
{
    private bool $success;
    private ?string $message;
    private ?Throwable $exception;


    public function __construct(bool $success, ?string $message = '', ?Throwable $exception = null)
    {
        $this->success = $success;
        $this->message = $message;
        $this->exception = $exception;
    }

    public function isSuccess() : bool
    {
        return $this->success;
    }

    public function getOutput() : string
    {
        return $this->message;
    }

    public function hasException() : bool
    {
        return $this->exception !== null;
    }

    public function getException() : ?Throwable
    {
        return $this->exception;
    }

    public function toArray() : array
    {
        return [
            'success' => $this->success,
            'message' => $this->message,
        ];
    }

    public function toJson() : string
    {
        return json_encode($this->toArray());
    }
}
