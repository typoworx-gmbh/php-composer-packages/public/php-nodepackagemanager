<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager\Bridges;

class NpmPackageManager extends AbstractPackageManager
{
    public function installPackage(string $packageName) : PackageManagerResponse
    {
        return $this->runCommand(sprintf(
            'npm install --prefix %s/npm-test %s',
            escapeshellarg($this->path),
            escapeshellarg($packageName)
        ));
    }

    public function removePackage(string $packageName) : PackageManagerResponse
    {
        return $this->runCommand(sprintf(
            'npm uninstall %s',
            escapeshellarg($packageName)
        ));
    }

    public function updatePackage(string $packageName) : PackageManagerResponse
    {
        return $this->runCommand(sprintf(
            'npm update %s',
            escapeshellarg($packageName)
        ));
    }
}
