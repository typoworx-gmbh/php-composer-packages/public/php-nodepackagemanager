<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager\Bridges;

interface PackageManagerInterface
{
    public function setPath(string $path) : void;
    public function getPath() : string;

    public function installPackage(string $packageName) : PackageManagerResponse;
    public function removePackage(string $packageName) : PackageManagerResponse;
    public function updatePackage(string $packageName,) : PackageManagerResponse;
}
