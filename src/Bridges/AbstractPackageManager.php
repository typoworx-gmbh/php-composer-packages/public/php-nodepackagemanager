<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager\Bridges;

use Symfony\Component\Process\Process;

abstract class AbstractPackageManager implements PackageManagerInterface
{
    private float $cliTimeout = 900;

    protected ?string $path = '/tmp/';


    public function setPath(string $path) : void
    {
        $this->path = $path;
    }

    public function getPath() : string
    {
        return $this->path;
    }

    protected function runCommand(string $command, ?array $env = []) : PackageManagerResponse
    {
        $process = Process::fromShellCommandline($command, $this->path);
        $process->setWorkingDirectory($this->path);
        $process->setEnv($env ?? []);
        $process->setTimeout($this->cliTimeout);

        try
        {
            $process->run();
        }
        catch (\Throwable $e)
        {
            return new PackageManagerResponse(false, null, $e);
        }

        return new PackageManagerResponse($process->isSuccessful(), $process->getOutput());
    }
}
