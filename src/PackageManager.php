<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager;

use TYPOworx\NodePackageManager\Bridges\PackageManagerInterface;
use TYPOworx\NodePackageManager\Bridges\PackageManagerResponse;

class PackageManager
{
    private PackageManagerInterface $packageManager;
    private ?PackageManagerResponse $packageManagerResponse;


    public function __construct(PackageManagerInterface $packageManager)
    {
        $this->packageManager = $packageManager;
    }

    public function installPackage($packageName) : bool
    {
         $this->packageManagerResponse = $this->packageManager->installPackage($packageName);

         return $this->packageManagerResponse->isSuccess();
    }

    public function removePackage($packageName) : bool
    {
        $this->packageManagerResponse =  $this->packageManager->removePackage($packageName);

        return $this->packageManagerResponse->isSuccess();
    }

    public function updatePackage($packageName) : bool
    {
        $this->packageManagerResponse =  $this->packageManager->updatePackage($packageName);

        return $this->packageManagerResponse->isSuccess();
    }
}
