<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager\Tests\Units;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\Process;
use TYPOworx\NodePackageManager\Bridges\PackageManagerInterface;
use TYPOworx\NodePackageManager\Bridges\PackageManagerResponse;

abstract class AbstractGenericPackageManagerTestSetup extends TestCase
{
    private string $testDirectory = __DIR__ . '/../../Build/tests/';
    private string $packageManagerCommand = 'npm';
    protected string $nodeTestPackage = 'hello-world-npm';

    protected ?PackageManagerInterface $packageManager;

    public function setUp(): void
    {
        $this->cleanUp();

        if (!is_dir($this->testDirectory))
        {
            mkdir($this->testDirectory);
        }

        $this->checkPackageManagerAvailability();
    }

    public function tearDown(): void
    {
        // Clean up the test directory after running tests
        //$this->cleanUp();
    }

    protected function packageManagerFactory(string $packageManagerClassName)
    {
        $this->packageManager = new $packageManagerClassName();;
        $this->packageManager->setPath($this->testDirectory);
    }

    public function testPackageManagerInterface() : void
    {
        $this->assertTrue($this->packageManager instanceof PackageManagerInterface);
    }

    protected function testPackageManagerResponse(?PackageManagerResponse $response) : void
    {
        $this->assertTrue($response instanceof PackageManagerResponse);


        $this->assertFalse($response->hasException(), $response->getException()?->getMessage() ?? '');
        $this->assertNull($response->getException(), $response->getException()?->getMessage() ?? '');
        $this->assertTrue($response->isSuccess(), $response->getOutput());
    }

    protected function setPackageManagerCommand(string $packageManagerCommand) : void
    {
        $this->packageManagerCommand = $packageManagerCommand;
    }

    private function cleanUp(): void
    {
        return;

        // Clean up the test directory
        if (is_dir($this->testDirectory))
        {
            $this->removeDirectory($this->testDirectory);
        }
    }

    private function removeDirectory(string $dir): void
    {
        $files = array_diff(scandir($dir), ['.', '..']);
        foreach ($files as $file)
        {
            (is_dir("$dir/$file")) ? $this->removeDirectory("$dir/$file") : unlink("$dir/$file");
        }
        rmdir($dir);
    }

    private function isCommandAvailable(): bool
    {
        $process = new Process([$this->packageManagerCommand, '--version']);
        $process->run();

        return $process->isSuccessful();
    }

    private function checkPackageManagerAvailability(): void
    {
        if (empty($this->packageManagerCommand) || !$this->isCommandAvailable())
        {
            $this->markTestSkipped(sprintf('%s is not available.', $this->packageManagerCommand));
        }
    }
}
