<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager\Tests\Units;

use TYPOworx\NodePackageManager\Bridges\NpmPackageManager;

class NpmPackageManagerTest extends AbstractGenericPackageManagerTestSetup
{
    public function setUp(): void
    {
        parent::setUp();
        $this->setPackageManagerCommand('npm');
        $this->packageManagerFactory(NpmPackageManager::class);
    }

    public function testInstallPackage(): void
    {
        $response = $this->packageManager->installPackage($this->nodeTestPackage ?? null);

        $this->testPackageManagerResponse($response);
    }

    public function testRemovePackage(): void
    {
        $response = $this->packageManager->removePackage($this->nodeTestPackage ?? null);

        $this->testPackageManagerResponse($response);
    }

    public function testUpdatePackage(): void
    {
        $response = $this->packageManager->updatePackage($this->nodeTestPackage ?? null);

        $this->testPackageManagerResponse($response);
    }
}
