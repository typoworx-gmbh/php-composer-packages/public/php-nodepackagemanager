<?php
declare(strict_types=1);
namespace TYPOworx\NodePackageManager\Tests\Units;

use TYPOworx\NodePackageManager\PackageManager;
use TYPOworx\NodePackageManager\Bridges\YarnPackageManager;

class YarnPackageManagerTest extends AbstractGenericPackageManagerTestSetup
{
    protected ?string $packageManagerClass = YarnPackageManager::class;

    public function setUp(): void
    {
        parent::setUp();

        $this->setPackageManagerCommand('yarn');
        $this->packageManagerFactory(YarnPackageManager::class);
    }

    public function testInstallPackage(): void
    {
        $response = $this->packageManager->installPackage($this->nodeTestPackage ?? null);

        $this->assertTrue($response->isSuccess());
    }

    public function testRemovePackage(): void
    {
        $response = $this->packageManager->removePackage($this->nodeTestPackage ?? null);

        $this->testPackageManagerResponse($response);
    }

    public function testUpdatePackage(): void
    {
        $response = $this->packageManager->updatePackage($this->nodeTestPackage ?? null);

        $this->testPackageManagerResponse($response);
    }
}
